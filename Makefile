SUBDIRS =\
	libcommon\
	yacc\
	lex\
	man \
	awk \
	stty\
	fmt\
	hd\
	bc\
	dc \
	file \
	pgrep\
	patch\
	users \
	diff\
	tar \

include dir.mk

bc lex awk: yacc

dc cp  tee file find date cat mkdir diff fmt hd tar pgrep stty: libcommon


