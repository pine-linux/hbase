/* see LICENSE file for copyright and license details */
static char *manpaths[] = { /* must end with '/' */
	"/usr/man/",
	"/usr/share/man/",
	"/usr/local/man/",
	"/usr/local/share/man/",
	"/opt/man/"
};

static char *pagers[] = {
   "cat"
};

#define TABSTOP 8
#define MANWIDTH 80
